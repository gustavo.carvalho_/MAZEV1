#include "bonus.hpp"

using namespace std;

Bonus::Bonus(){}

int Bonus::getPonto(){
  return ponto;
}
void Bonus::setPonto(int valor){
  this-> ponto += valor;
}
int Bonus::getA(){
  return a;
}
void Bonus::setA(int a){
  this-> a = a;
}
int Bonus::getB(){
  return b;
}
void Bonus::setB(int b){
  this-> b = b;
}
int Bonus::getC(){
  return c;
}
void Bonus::setC(int c){
  this-> c = c;
}
int Bonus::getD(){
  return d;
}
void Bonus::setD(int d){
  this-> d = d;
}
int Bonus::getE(){
  return e;
}
void Bonus::setE(int e){
  this-> e = e;
}

void Bonus::printarBonus(int py, int px){
  if(this->getA()==1){
    if(py==16 && px==49){
      move(16,49);
      printw(" ");
      this->setA(2);
      this->setPonto(1);
    }else{
      move(16,49);
      printw("$");
      refresh();
    }
  }

  if(this->getB()==1){
    if(py==3 && px==7){
      move(3,7);
      printw(" ");
      this->setB(2);
      this->setPonto(1);
    }else{
      move(3,7);
      printw("$");
      refresh();
    }
  }

  if(this->getC()==1){
    if(py==19 && px==1){
      move(19,1);
      printw(" ");
      this->setC(2);
      this->setPonto(1);
    }else{
      move(19,1);
      printw("$");
      refresh();
    }
  }

  if(this->getD()==1){
    if(py==9 && px==22){
      move(9,22);
      printw(" ");
      this->setD(2);
      this->setPonto(1);
    }else{
      move(9,22);
      printw("$");
      refresh();
    }
  }

  if(this->getE()==1){
    if(py==3 && px==42){
      move(3,42);
      printw(" ");
      this->setE(2);
      this->setPonto(1);
    }else{
      move(3,42);
      printw("$");
      refresh();
    }
  }
}

void Bonus::printarPonto(){
  move(29,8);
  printw("%d",this->getPonto());
  refresh();
}
