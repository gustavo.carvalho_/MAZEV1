#include "player.hpp"

using namespace std;

Player::Player(int px, int py){
  this->setPx(px);
  this->setPy(py);
}

void Player::printarPlayer(){
  move(this->getPx(),this->getPy());
  printw("@");
  refresh();

}

void Player::movimento(){

  char direcao='x';
  direcao=getch();

  //inicialização do arquivo map///
  vector<string>tela;
	ifstream mapFile("./doc/map.txt");
	string line;

	while (!mapFile.eof()) {
		getline(mapFile, line);
		line += '\n';
		tela.push_back(line);
  }
  mapFile.close();
  //arquivo inicializado

  //direção
  if(direcao=='w'){
    if (tela[this->getPx()-1][this->getPy()]==' ') {
      this->setPx(-1);
    }
  }
  if(direcao=='s'){
    if (tela[this->getPx()+1][this->getPy()]==' ') {
      this->setPx(1);
    }
  }
  if(direcao=='d'){
    if (tela[this->getPx()][this->getPy()+1]==' ') {
      this->setPy(1);
    }
  }
  if(direcao=='a'){
    if (tela[this->getPx()][this->getPy()-1]==' ') {
      this->setPy(-1);
    }
  }
}

int Player::estado(){
  if((this->getPx()==21) && (this->getPy()==50)){
   return 2;
  }else{
    return 1;
  }

}

void Player::printarCoordenadas(){
  move(28,17);
  printw("%d",this->getPy());
  move(28,22);
  printw("%d",this->getPx());
  refresh();
}
