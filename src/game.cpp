#include "game.hpp"

using namespace std;

Game::Game(){
  this->setPx(0);
  this->setPy(0);
}

Game::Game(int px, int py){
  this->setPx(px);
  this->setPy(py);
}

int Game::getPx(){
  return px;
}

void Game::setPx(int valor){
  this-> px += valor;
}

int Game::getPy(){
  return py;
}

void Game::setPy(int valor){
  this-> py += valor;
}
