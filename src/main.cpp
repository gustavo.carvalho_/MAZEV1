#include "game.hpp"
#include "player.hpp"
#include "map.hpp"
#include "bonus.hpp"
#include "trap.hpp"

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <ncurses.h>

int main() {
  //iniciando os objetos
  Map * map = new Map();
  Player * player = new Player(1,0);
  Bonus * bonus = new Bonus();
  Trap * trap = new Trap();
  bonus->setA(1);
  bonus->setB(1);
  bonus->setC(1);
  bonus->setD(1);
  bonus->setE(1);
  trap->setVida(2);
  //variavel que determina o loop continuo
  int game=1;
  int a=0;
  //loop do game
  while(game==1){
    initscr();
    curs_set(0);
    map->printarMapa();
    bonus->printarBonus(player->getPx(),player->getPy());
    if(a<4)
    trap->printarTrap();
    player->printarPlayer();
    player->printarCoordenadas();
    bonus->printarPonto();
    trap->printarVida();
    player->movimento();
    trap->verificaTrap(player->getPx(),player->getPy());
    if(trap->estado()==3)
    game=3;
    if(player->estado()==2)
    game=2;
    a++;
    if(a>11)
    a=0;
    refresh();
    clear();
    endwin();
  }
  clear();

  initscr();
  curs_set(0);

  if(game==3){
    printw("\n\n\n              VOCE PERDEU!!!! HAHAHA >:D ");
    getch();
  }
  if(game==2){
    printw("\n\n\n              PARABENS! VOCE CONSEGUIU SAIR!!!");
    printw("\n              SEU PLACAR FOI DE %d PONTOS!",bonus->getPonto());
    getch();
  }

  refresh();
  endwin();

  return 0;
}
