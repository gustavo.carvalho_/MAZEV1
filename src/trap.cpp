#include "trap.hpp"

using namespace std;

Trap::Trap(){}

int Trap::getVida(){
  return vida;
}
void Trap::setVida(int valor){
  this->vida+= valor;
}
int Trap::getD(){
  return d;
}
void Trap::setD(int d){
  this->d=d;
}
void Trap::printarTrap(){
  move(2,1);//1
  printw("X");
  move(5,2);//2
  printw("X");
  move(6,6);//3
  printw("X");
  move(7,13);//4
  printw("X");
  move(9,10);//5
  printw("X");
  move(11,11);//6
  printw("X");
  move(12,2);//7
  printw("X");
  move(20,2);//8
  printw("X");
  move(22,1);//9
  printw("X");
  move(18,15);//10
  printw("X");
  move(20,29);//11
  printw("X");
  move(20,35);//12
  printw("X");
  move(22,38);//13
  printw("X");
  move(22,44);//14
  printw("X");
  move(20,43);//15
  printw("X");
  move(21,47);//16
  printw("X");
  move(14,48);//17
  printw("X");
  move(11,49);//18
  printw("X");
  move(13,41);//19
  printw("X");
  move(15,40);//20
  printw("X");
  move(17,36);//21
  printw("X");
  move(13,37);//22
  printw("X");
  move(9,38);//23
  printw("X");
  move(5,33);//24
  printw("X");
  move(7,28);//25
  printw("X");
  move(3,26);//26
  printw("X");
  refresh();
}

void Trap::printarVida(){
  move(30,8);
  printw("%d",this->getVida());
}

void Trap::verificaTrap(int py,int px){
  if(py==2 && px==1){
  this->setVida(-1);
  }
  if(py==5 && px==2){
  this->setVida(-1);
  }
  if(py==6 && px==6){
  this->setVida(-1);
  }
  if(py==7 && px==13){
  this->setVida(-1);
  }
  if(py==9 && px==10){
  this->setVida(-1);
  }
  if(py==11  && px==11){
  this->setVida(-1);
  }
  if(py==12 && px==2){
  this->setVida(-1);
  }
  if(py==20  && px==2){
  this->setVida(-1);
  }
  if(py==22  && px==1){
  this->setVida(-1);
  }
  if(py==18 && px==15){
  this->setVida(-1);
  }
  if(py==20 && px==29){
  this->setVida(-1);
  }
  if(py==20 && px==35){
  this->setVida(-1);
  }
  if(py==22 && px==38){
  this->setVida(-1);
  }
  if(py==22 && px==44){
  this->setVida(-1);
  }
  if(py==20 && px==43){
  this->setVida(-1);
  }
  if(py==21 && px==47){
  this->setVida(-1);
  }
  if(py==14 && px==48){
  this->setVida(-1);
  }
  if(py==11 && px==49){
  this->setVida(-1);
  }
  if(py==13 && px==41){
  this->setVida(-1);
  }
  if(py==15 && px==40){
  this->setVida(-1);
  }
  if(py==17 && px==36){
  this->setVida(-1);
  }
  if(py==13 && px==37){
  this->setVida(-1);
  }
  if(py==9 && px==38){
  this->setVida(-1);
  }
  if(py==5 && px==33){
  this->setVida(-1);
  }
  if(py==7 && px==28){
  this->setVida(-1);
  }
  if(py==3 && px==26){
  this->setVida(-1);
  }
}

int Trap::estado(){
  if(this->getVida()<1){
   return 3;
  }else{
    return 1;
  }
}
