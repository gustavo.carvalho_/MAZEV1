#ifndef GAME_HPP
#define GAME_HPP

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <ncurses.h>

using namespace std;

class Game{
private:
  int px,py;
public:
  Game();
  Game(int px,int py);
  int getPx();
  void setPx(int valor);
  int getPy();
  void setPy(int valor);

};

#endif
