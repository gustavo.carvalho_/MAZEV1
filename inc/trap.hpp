#ifndef TRAP_HPP
#define TRAP_HPP

#include "game.hpp"

class Trap: public Game{
private:
  int vida;
  int d;
public:
  Trap();
  void printarTrap();
  void printarVida();
  void verificaTrap(int py, int px);
  int estado();
  int getVida();
  void setVida(int valor);
  int getD();
  void setD(int d);

};

#endif
