#ifndef BONUS_HPP
#define BONUS_HPP

#include "game.hpp"

class Bonus: public Game{
private:
  int ponto;
  int a,b,c,d,e;

public:
  Bonus();
  void printarBonus(int y,int x);
  void printarPonto();
  int getPonto();
  void setPonto(int valor);
  int getA();
  void setA(int a);
  int getB();
  void setB(int b);
  int getC();
  void setC(int c);
  int getD();
  void setD(int d);
  int getE();
  void setE(int e);
};

#endif
