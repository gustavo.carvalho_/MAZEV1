#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "game.hpp"

class Player : public Game{
public:
  Player();
  Player(int px,int py);
  void movimento();
  void printarPlayer();
  int estado();
  void printarCoordenadas();
};

#endif
